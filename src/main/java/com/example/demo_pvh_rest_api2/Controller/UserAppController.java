package com.example.demo_pvh_rest_api2.Controller;


import com.example.demo_pvh_rest_api2.model.UserApp;
import com.example.demo_pvh_rest_api2.model.dto.UserAppDto;
import com.example.demo_pvh_rest_api2.model.request.UserAppRequest;
import com.example.demo_pvh_rest_api2.model.response.UserAppResponse;
import com.example.demo_pvh_rest_api2.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/v1")
public class UserAppController {
    private final UserService userService;

    public UserAppController(UserService userService) {
        this.userService = userService;
    }


//    @GetMapping("/welcome")
//    public String welcomeEndPoint() {
//        return "Welcome To App";
//    }
//
//    @GetMapping("/user")
//    public String welcomeUser() {
//        return "Welcome To User";
//    }
//
//    @GetMapping("/admin")
//    public String welcomeAdmin() {
//        return "Welcome To Admin";
//    }

    @PostMapping("/post-user")
    public ResponseEntity<UserAppResponse<UserAppDto>> register(@RequestBody UserAppRequest userAppRequest) {
        UserAppResponse<UserAppDto> userAppResponse = UserAppResponse.<UserAppDto>builder()
                .message("Register New User Successful")
                .payload(userService.addNewUser(userAppRequest))
                .dateTime(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(userAppResponse);
    }

    @GetMapping("/get-user-by-id/{id}")
    public ResponseEntity<UserAppResponse<UserAppDto>> getUserById(@PathVariable("id") Integer id) {
        UserAppResponse<UserAppDto> userAppResponse = UserAppResponse.<UserAppDto>builder()
                .message("Found User Successful")
                .payload(userService.getUserById(id))
                .dateTime(LocalDateTime.now())
                .build();
        return ResponseEntity.ok().body(userAppResponse);
    }
}
