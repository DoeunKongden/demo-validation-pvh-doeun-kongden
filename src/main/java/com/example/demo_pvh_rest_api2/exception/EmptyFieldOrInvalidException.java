package com.example.demo_pvh_rest_api2.exception;

public class EmptyFieldOrInvalidException extends RuntimeException{
    public EmptyFieldOrInvalidException(String message){
        super(message);
    }
}
