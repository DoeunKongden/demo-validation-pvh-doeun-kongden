package com.example.demo_pvh_rest_api2.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice //This will make this class turn into a global exception
public class GlobalExceptionHandler {

    //Creating a method for taking any exception that other class throw at it

    @ExceptionHandler(EmptyFieldOrInvalidException.class)
        //this annotation will let this method know the specific exception of the class that pass it
    ProblemDetail emptyOrInvalidHandler(EmptyFieldOrInvalidException exception) {

        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.BAD_REQUEST, exception.getMessage()
        ); //this method let us throw a status code and problem detail

        problemDetail.setTitle("Empty Field !!");
        problemDetail.setType(URI.create("localhost:8080/error/bad-request")); // for going to a document page to see why it a bad request
        return problemDetail;
    }

    @ExceptionHandler(UserNotFoundException.class)
    ProblemDetail userNotFoundHandler(UserNotFoundException exception) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND, exception.getMessage()
        ); //this method let us throw a status code and problem detail

        problemDetail.setTitle("User Not Found!!");
        problemDetail.setType(URI.create("localhost:8080/error/not-found")); // for going to a document page to see why it a bad request
        return problemDetail;
    }
}
