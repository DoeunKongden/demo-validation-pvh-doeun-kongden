package com.example.demo_pvh_rest_api2.mapper;


import com.example.demo_pvh_rest_api2.model.UserApp;
import com.example.demo_pvh_rest_api2.model.dto.UserAppDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {


    //This will generate a class that implement the UserMapper Interface for us
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    @Mapping(source = "fullName",target = "fullName") //we using mapping anotation from mapstruct to manually map data by oursevle
    UserAppDto toUserAppDto(UserApp userApp);
}
