package com.example.demo_pvh_rest_api2.service.serviceImplement;

import com.example.demo_pvh_rest_api2.exception.EmptyFieldOrInvalidException;
import com.example.demo_pvh_rest_api2.exception.UserNotFoundException;
import com.example.demo_pvh_rest_api2.mapper.UserMapper;
import com.example.demo_pvh_rest_api2.model.UserApp;
import com.example.demo_pvh_rest_api2.model.constant.Gender;
import com.example.demo_pvh_rest_api2.model.dto.UserAppDto;
import com.example.demo_pvh_rest_api2.model.request.UserAppRequest;
import com.example.demo_pvh_rest_api2.repository.UserAppRepository;
import com.example.demo_pvh_rest_api2.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserAppImp implements UserService {


    private final UserAppRepository userAppRepository;

    public UserAppImp(UserAppRepository userAppRepository) {
        this.userAppRepository = userAppRepository;
    }

    @Override
    public UserAppDto addNewUser(UserAppRequest userAppRequest) {
        UserApp userApp = userAppRepository.register(userAppRequest);

        if (userAppRequest.getFullName().isEmpty()) {
            throw new EmptyFieldOrInvalidException("Full Name Can't Be Empty");
        } else if (userAppRequest.getEmail().isEmpty()) {
            throw new EmptyFieldOrInvalidException("Email Can't Be Empty");
        } else if (userAppRequest.getAddress().isEmpty()) {
            throw new EmptyFieldOrInvalidException("Address Can't Be Empty");
        } else if (userAppRequest.getGender().isEmpty()) {
            throw new EmptyFieldOrInvalidException("Gender Can't Be Empty");
        } else if (userAppRequest.getPassword().isEmpty()) {
            throw new EmptyFieldOrInvalidException("Password Can't Be Empty");
        } else {
            boolean isGender = false;
            for (Gender gender : Gender.values()) {
                if (userAppRequest.getGender().equalsIgnoreCase(gender.name())) {
                    isGender = true;
                    break;
                }
            }
            if (!isGender) {
                throw new EmptyFieldOrInvalidException("Invalid Gender");
            }
        }


//        Menually Map data response customizing
//        UserAppDto userAppDto = new UserAppDto(userApp.getUserId(),
//        userApp.getFullName(), userApp.getEmail(), userApp.getAddress(), userApp.getGender());
//        return userAppDto;

        //using map struct to auto map
        return UserMapper.INSTANCE.toUserAppDto(userApp);
    }

    @Override
    public UserAppDto getUserById(Integer id) {
        UserApp userApp = userAppRepository.getUserById(id);
        if(userApp == null){
            throw new UserNotFoundException("This User ID " + id + "Is Not Found");
        }else {
            return UserMapper.INSTANCE.toUserAppDto(userAppRepository.getUserById(id));
        }
    }
}
