package com.example.demo_pvh_rest_api2.service;

import com.example.demo_pvh_rest_api2.model.UserApp;
import com.example.demo_pvh_rest_api2.model.dto.UserAppDto;
import com.example.demo_pvh_rest_api2.model.request.UserAppRequest;

public interface UserService {
    UserAppDto addNewUser(UserAppRequest userAppRequest);

    UserAppDto getUserById(Integer id);
}
