package com.example.demo_pvh_rest_api2.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAppRequest {
    private String fullName;
    private String email;
    private String password;
    private String address;
    private String gender;
}
