package com.example.demo_pvh_rest_api2.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAppDto {

    //This class is use for responding to user
    private Integer userId;
    private String fullName;
    private String email;
    private String address;
    private String gender;
}
