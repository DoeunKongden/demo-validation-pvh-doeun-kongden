package com.example.demo_pvh_rest_api2.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserApp {

    private Integer userId;
    private String fullName;
    private String email;
    private String password;
    private String address;
    private String gender;
}
