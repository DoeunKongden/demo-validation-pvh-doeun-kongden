package com.example.demo_pvh_rest_api2.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAppResponse<T> {
    private String message;
    private T payload;
    private LocalDateTime dateTime;
}
