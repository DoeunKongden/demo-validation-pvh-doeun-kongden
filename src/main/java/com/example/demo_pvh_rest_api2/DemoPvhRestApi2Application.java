package com.example.demo_pvh_rest_api2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPvhRestApi2Application {
    public static void main(String[] args) {
        SpringApplication.run(DemoPvhRestApi2Application.class, args);
    }

}
