package com.example.demo_pvh_rest_api2.repository;

import com.example.demo_pvh_rest_api2.model.UserApp;
import com.example.demo_pvh_rest_api2.model.dto.UserAppDto;
import com.example.demo_pvh_rest_api2.model.request.UserAppRequest;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserAppRepository {

    @Select("""
            Insert into user_tb(full_name,email,password,address,gender) 
            values (#{userAppRequest.fullName},#{userAppRequest.email},#{userAppRequest.password},
            #{userAppRequest.address},
            #{userAppRequest.gender}) returning *
            """)
    @Results(id = "userMap", value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "fullName", column = "full_name"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(property = "address", column = "address"),
            @Result(property = "gender", column = "gender")
    })
    UserApp register(@Param("userAppRequest") UserAppRequest userAppRequest);


    @Select("""
            select from user_tb where user_id = #{id}
            """)
    @ResultMap("userMap")
    UserApp getUserById(Integer id);

}
